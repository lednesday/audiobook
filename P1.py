#!/usr/bin/env python3

"""One-hand audiobook reader
    Audiobook reader controlled with one hand, using the keys j, k, l, ;, and <space>

    Author: Lindsay Marean

    Attribution: I never would have been able to write this without extensive help from
    Chris Jones, who especially helped me to rough in and troubleshoot my attempt
    at a state design pattern
    I also had some guidance from Anthony Hornof, University of Oregon

    Project specifications: https://classes.cs.uoregon.edu/20F/cis443/P1.html

    Takes an optional command line argument of one-word username;
    if no username entered then new user is default,
    and user data is lost upon program exit.

    TODO: Focus mode hasn't been implement yet
    audio file formats cause lots of problems; wav doesn't always allow for get_pos(),
    so skipping back/forward will probably cause a crash
    user preferences can't be adjusted yet
    most recent spot isn't actually saved; this is also due to audio file format problems
    context should be a class, not a dict
    too many things are hard-coded in so that a single book and chapter will play

    TODO: Last edited on October 18, 2020
    """

import pygame
import sys  # needed to take commandline username argument to store user data


def start_pygame():
    """starts pygame
    Uses some code from Anthony Hornof's examples
    pygame is documented here: https://www.pygame.org/docs/index.html
    """
    start_pygame = pygame.init()  # variable is for testing purposes
    screen = pygame.display.set_mode((400, 100))
    screen.fill((50, 100, 200))  # turns blue
    pygame.font.init()  # not needed for audio-only, but yes for text display I think
    myfont = pygame.font.Font(pygame.font.get_default_font(), 20)
    text = myfont.render("One-hand Audio Reader", True, (200, 50, 50))
    screen.blit(text, (0, 0))
    # would be nice to add "Welcome to the one-hand audiobook reader!" text
    pygame.mixer.init(channels=1)  # for playing audio
    # new_start_time = 0  # Used to advance the play()


class UserSettings:
    """Manages user-specific settings

    username is the unique identifier of a user, needed to store user data in a text file for next use
    new is True when the username isn't already stored in userlist.txt, False otherwise
    book is the book this user is currently reading. It actually holds a string containing the directory name of this book's audiofiles, within the directory books.
    It would be nice to store "most recent" information for each book the user reads, but not in this version.
    chapter is the specific audiofile that the user has most recently been reading
    most_recent is the timestamp of the most recent place the reader was reading
    focus is a boolean indicating if focus mode is on or off
    last_focus_point is the timestamp of the most recent focus point for "focus check" functionality
    skip_interval allows the user to set how far forward or back to skip in reading mode
    focus_interval allws the user to set how often the focus pause should happen
    """

    def __init__(self, username, new, book, chapter, most_recent, focus, last_focus, skip_interval, focus_interval):
        self.username = username  # unique one-word user identifier
        self.new = new  # is this a new user?
        self.book = book  # directory of book
        self.chapter = chapter  # audio filename
        self.most_recent = most_recent  # timestamp of most recent place in the book
        self.focus = focus  # focus mode on?
        self.last_focus_point = last_focus  # focus point timestamp
        self.skip_interval = skip_interval  # in seconds
        self.focus_interval = focus_interval  # in minutes


def load_preferences():
    """returns user, a UserSettings object
    If command line argument is a single word, treats as username and attempts to retrieve saved user data
    If this is a new username, addes to userlist.txt and creates a new textfile in users directory to store user-specific data
    If no command line argument, then treats as a new user and does not save any settings upon program exit
    """
    is_new = True
    new_username = ""
    num_pref = 8  # number of user preferences saved
    if len(sys.argv) == 2:  # checking to see if a username was entered
        username = sys.argv[1]
        with open("userlist.txt", 'r') as users_file:
            for line in users_file:
                if line == username:  # username exists
                    is_new = False
        # text file with this user's saved preferences
        filepath = "users/" + username + ".txt"
        if not is_new:
            # retrieve user data
            with open(filepath, "r") as userdata:
                lines = userdata.readlines()
            # check to see if correct number of lines were read
            print("lines: ", len(lines))
            if len(lines) == num_pref:
                # create UserSettings object with saved values
                user = UserSettings(
                    lines[0].strip(), False, lines[1].strip(), lines[2].strip(), float(lines[3]), lines[4], int(lines[5]), int(lines[6]), int(lines[7]))
            else:
                print("User data file corruption. Exiting program")
                sys.exit()
        else:  # this is a new user who needs a new textfile to store saved preferences
            # add username to userlist
            with open("userlist.txt", "a") as users_file:
                users_file.write("\n" + username)
            # make a (blank for now) text file to store this user's preferences
            open(filepath, 'w').close()
            new_username = username
    if is_new:
        if len(sys.argv) > 2:
            print("Username must be a single word. Initiating new user session.")
        user = UserSettings(new_username, True, "", "", 0, False, 0, 5, 5)
    return user


def play_file(pathname):
    """boilerplate to stream audio files into the pygame interface

    Args:
        pathname (string): filepath to specific audio file
    """
    pygame.mixer.music.load(pathname)
    pygame.mixer.music.play()
    # this makes it impossible to shortcut an audio while it's playing
    # while pygame.mixer.music.get_busy():
    #     pass


class _navigate:
    """This is an abstract class representing a state, for the other "state" classes to use
    context is a dictionary with a UserSettings object and a _navigate object (or one of its child classes)
    num_options is the number of options in a given menu
    Defines the behavior of different keystrokes
    do_the_stuff causes any class methods that need to be called, to be called

    Attribution: There's no way I could have figured out how to implement it this way,
    using the state design pattern, without the help of Chris Jones. And also Gammet et al., the "Gang of Four."

    """

    def __init__(self, context):
        self.context = context
        self.menu_index = 0
        self.num_options = 0

    def j(self):
        if self.menu_index < self.num_options - 1:
            self.menu_index += 1
        else:
            self.menu_index = 0
        play_file(self.menu[self.menu_index][0])

    def k(self):
        if self.menu_index > 0:
            self.menu_index -= 1
        else:
            self.menu_index = self.num_options - 1
        play_file(self.menu[self.menu_index][0])

    def space(self):
        # creates a new instance of selected class
        temp = self.menu[self.menu_index][1](self.context)
        self.context["current menu"] = temp
        temp.enter()

    def do_the_stuff(self):
        """a child class can put function calls here
        """
        pass

    def enter(self):
        """plays intro file to object menu and calls do_the_stuff
        """
        print("Entering", self)  # for testing
        play_file(self.audio)
        self.do_the_stuff()


class first_use(_navigate):
    """landing page for first-time user
    Plays introduction, then goes to navigation instructions

    Args: _navigate: parent class
    """

    def __init__(self, context):
        super().__init__(context)
        self.audio = "audio_assets/first_use.wav"

    def space(self):
        self.context["current menu"] = navigation_instructions(self.context)
        self.context["current menu"].enter()


class main_menu(_navigate):
    """main landing page for reader; prototypical menu class

    Args:
        _navigate: parent class
    """

    def __init__(self, context):
        self.menu = [("audio_assets/exit_reader.wav", exit_reader), ("audio_assets/replay_instructions.wav", navigation_instructions), ("audio_assets/resume_reading.wav", resume_reading),
                     ("audio_assets/new_book.wav", select_book), ("audio_assets/navigate_book.wav", navigate_current), ("audio_assets/set_preferences.wav", set_preferences)]
        super().__init__(context)
        self.num_options = 6
        self.audio = "audio_assets/main_menu.wav"


class resume_reading(_navigate):
    """enters "reading mode"
    Traits of reading mode:
        space is play/pause (toggle)
        j advances a set amount (can be changes in user preferences)
        k goes backward a set amounts
        When "focus checking" is on, playback pauses automatically at fixed intervals;
        default interval is 4 minutes, but this can be adjusted by the user.
        User can then continue playback with j, or repeat the last focus interval with k.
        l or ; pauses play, stores current timestamp in user.most_recent, and returns to main menu

    TODO:
    Focus mode functionality has yet to be implemented
    Skipping forward and backward cause a crash because of audio file format issues
    Is supposed to save the reader's place, but doesn't
    """

    def __init__(self, context):
        self.menu = [(), ]
        super().__init__(context)
        self.audio = "audio_assets/reading_mode.wav"
        self.paused = False

    def start_play(self, context):
        # temporarily hard-coded to play a single book, so it starts with hard-coded first file
        if self.context["user"].chapter == "":
            self.context["user"].chapter = "books/DOET/001-DesignEveryday_Preface_2002.ogg"
        print("chapter: ", self.context["user"].chapter)
        pygame.mixer.music.load(self.context["user"].chapter)
        print("start play most recent: ", self.context["user"].most_recent)
        pygame.mixer.music.play(start=self.context["user"].most_recent)

    def j(self):
        # will probably crash if skip goes past end of recording
        print("j position: ", pygame.mixer.music.get_pos())
        print("j skip interval: ", self.context["user"].skip_interval)
        pygame.mixer.music.play(start=(
            (pygame.mixer.music.get_pos())/1000)+(self.context["user"].skip_interval/1000))

    def k(self):
        pygame.mixer.music.play(start=(
            (pygame.mixer.music.get_pos())/1000)-(self.context["user"].skip_interval/1000))

    def space(self):
        self.paused = not self.paused
        if self.paused:
            pygame.mixer.music.unpause()
        else:
            pygame.mixer.music.pause()
            # note pause time to save in user preferences
            self.update_time()
            print("paused at: ", self.context["user"].most_recent)

    def l(self):
        # note pause time to save in user preferences
        self.update_time
        self.context["current menu"] = main_menu(self.context)
        self.context["current menu"].enter()

    def semicolon(self):
        # note pause time to save in user preferences
        self.update_time
        self.context["current menu"] = main_menu(self.context)
        self.context["current menu"].enter()

    def do_the_stuff(self):
        if self.context["user"].new:
            play_file("audio_assets/reading_mode.wav")
            # for some reason if I don't do this busy loop the above won't play
            while pygame.mixer.music.get_busy():
                pass
        self.start_play(self.context)

    def update_time(self):
        """doesn't seem to be working
        """
        most_recent = self.context["user"].most_recent
        print("most recent in update time: ", most_recent)
        print("get pos in update time: ", pygame.mixer.music.get_pos())
        self.context["user"].most_recent = (
            pygame.mixer.music.get_pos()/1000) + most_recent


class navigation_instructions(_navigate):
    """plays the basic navigation instructions:
    j for forward/down, k, for back/up, and space to select
    """

    def __init__(self, context):
        self.menu = [("audio_assets/replay_instructions.wav", "navigation_instructions"),
                     ("audio_assets/return_to_main.wav", "main_menu"), ("audio_assets/exit_reader.wav", "exit_reader")]
        super().__init__(context)
        self.num_options = 3
        self.audio = "audio_assets/navigation_instructions.wav"

    def space(self):
        do_this = self.menu[self.menu_index][1]
        if do_this == "navigation_instructions":
            play_file(self.audio)
        elif do_this == "main_menu":
            self.context["current menu"] = main_menu(self.context)
            self.context["current menu"].enter()
        elif do_this == "exit_reader":
            self.context["current menu"] = exit_reader(self.context)
            exit_reader(self.context)
            self.context["current menu"].enter()


class select_book(_navigate):
    """Allows the user to select what book to read
    Currently there's only one choice
    """

    def __init__(self, context):
        self.menu = [("books/DOET/title.wav", "DOET"), ("audio_assets/return_to_main.wav",
                                                        "main_menu"), ("audio_assets/exit_reader.wav", "exit_reader")]
        super().__init__(context)
        self.num_options = 3
        self.audio = "audio_assets/select_book_intro.wav"

    def space(self):
        do_this = self.menu[self.menu_index][1]
        if do_this == "DOET":
            self.context["user"].book = "DOET"
        elif do_this == "main_menu":
            self.context["current menu"] = main_menu(self.context)
            self.context["current menu"].enter()
        elif do_this == "exit_reader":
            self.context["current menu"] = exit_reader(self.context)
            exit_reader(self.context)
            self.context["current menu"].enter()


class navigate_current(_navigate):
    """presents different sections of the book for the user to navigate to
    Each section is defined as a separate audiofile in a given book's directory
    Currently there's just one book (DOET), so this is hard-coded.
    Future version needs to load this metadata from a book's directory
    """

    def __init__(self, context):
        self.menu = [("books/DOET/ct_preface_2002.wav", "books/DOET/001-DesignEveryday_Preface_2002.ogg"), ("books/DOET/ct_preface_1988.wav", "books/DOET/002-DesignEveryday_Preface_1988.ogg"),
                     ("books/DOET/ct_chapter_1.wav", "books/DOET/003-DesignEveryday_Chapter_1.ogg"), ("books/DOET/ct_chapter_2.wav", "books/DOET/004-DesignEveryday_Chapter_2.ogg"), ("audio_assets/main_menu.wav",
                                                                                                                                                                                      "main_menu"), ("audio_assets/exit_reader.wav", "exit_reader")]
        super().__init__(context)
        self.num_options = 6
        self.audio = "audio_assets/navigate_book_intro.wav"

    def space(self):
        do_this = self.menu[self.menu_index][1]
        if do_this == "main_menu":
            self.context["current menu"] = main_menu(self.context)
            self.context["current menu"].enter()
        elif do_this == "exit_reader":
            self.context["current menu"] = exit_reader(self.context)
            self.context["current menu"].enter()
        else:
            self.context["user"].chapter = do_this
            self.context["current menu"] = resume_reading(self.context)
            self.context["current menu"].enter()


class set_preferences(_navigate):
    """user sets preferences
    Preferences are as follows:
    skip_interval: how far to jump ahead or back with j/k keys in reading mode
    focus_interval: how often to pause to check for focus
    focus_on: True if focus checking is on, False if it's disabled

    Not built yet; this is a stub
    """

    def __init__(self, context):
        self.menu = []
        super().__init__(context)
        self.audio = "audio_assets/preferences_disabled.wav"

    def space(self):
        self.context["current menu"] = main_menu(self.context)
        self.context["current menu"].enter()


class exit_reader(_navigate):
    """saves user data and plays an exit message
    Writes user data to dedicated text file in users directory
    """

    def __init__(self, context):
        super().__init__(context)
        self.audio = "audio_assets/exit.wav"

    def save_preferences(self, context):
        """writes user preferences to a file called username.txt
        """
        # save user preferences
        filepath = "users/" + self.context["user"].username.strip() + ".txt"
        with open(filepath, "w") as userdata:
            userdata.write(self.context["user"].username + '\n')
            userdata.write(self.context["user"].book + '\n')
            userdata.write(self.context["user"].chapter + '\n')
            userdata.write(str(self.context["user"].most_recent) + '\n')
            userdata.write(str(self.context["user"].focus))
            userdata.write(str(self.context["user"].last_focus_point) + '\n')
            userdata.write(str(self.context["user"].skip_interval) + '\n')
            userdata.write(str(self.context["user"].focus_interval).strip())
        print("preferences saved!")  # just checking

    def graceful_exit(self):
        play_file(self.audio)
        while pygame.mixer.music.get_busy():
            pass
        pygame.quit()
        quit()

    def do_the_stuff(self):
        self.save_preferences(self.context)
        self.graceful_exit()


def main():
    """starts up a pygame surface (is that the right term?) and gets the user to the main menu
    Listens for key presses or a click on the display
    """
    context = {"user": load_preferences()}
    start_pygame()  # initialize pygame
    clock = pygame.time.Clock()  # to limit framerate
    if context["user"].new:
        current_menu = first_use(context)
    else:
        current_menu = main_menu(context)
    context.update({"current menu": current_menu})
    current_menu.enter()
    keep_going = True
    while keep_going:
        for event in pygame.event.get():
            # if user clicks on close button on display
            if event.type == pygame.QUIT:
                keep_going = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_j:
                    context["current menu"].j()

                if event.key == pygame.K_k:
                    context["current menu"].k()

                if event.key == pygame.K_SPACE:
                    context["current menu"].space()

                if event.key == pygame.K_l:
                    context["current menu"].l()

                if event.key == pygame.K_SEMICOLON:
                    context["current menu"].semicolon()

        pygame.display.flip()  # something about flipping buffers
        clock.tick(60)
    pygame.quit()
    quit()


if __name__ == "__main__":
    main()
