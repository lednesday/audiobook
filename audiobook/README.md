My first README:

Summary: This is an audiobook reader designed to be controlled with the fingers on one hand. This could be a fancy control stick, but for now we use the keys j(index), k(middle), l(ring), ;(pinky), and <SPACE>(thumb) as analogs.

Configuration: The pygame library is required to be installed on your computer to run this program

Installation: Run P1.py to launch the program.
Also needed: userlist.txt, users, audio_assets, and books in same directory as P1.py
The books directory contains a directory for each book available to the audio reader
It should also contain (but doesn't yet) a books.txt file with a list of currently available titles
Each book directory will include audiofiles for that book, as well as audiofiles with the section (chapter) names, plus a sections.txt file with a list of the section titles and audiofile names

Operation: Run P1.py with an optional one-word username to have user data and preferences stored. 
There are two main modes: navigation mode and reading mode.
In navigation mode, an introduction to a menu is played, listing the available options. Users scroll down/forward with the j key and up/back wtih the k key. Use <SPACE> to select the highlighted menu item.
From the main menu, users can select a book, navigate within a book, resume reading where they last left off, adjust preferences, listen to the navigation instructions, and exit the program.
Preferences include focus mode on/off, focus interval, and skip interval.
In reading mode, <SPACE> toggles play/pause of the audiofile. j skips forward, and k skips backward. The default skipping time is 5s, but this can be adjusted in the user preferences. l or ; returns to the main menu at any time. If focus mode is on, the playback will pause automatically at certain intervals; 5m is the default, but this can be changed in user preferences. The user can resume play with the j key, or replay the last interval with the k key.

Specifications: WAV format seems most successful for audiofiles. See Installation above for book contents specifications.

Authorship: Written by Lindsay Marean, lmarean@bensay.org. This project will be publicly accessible at https://bitbucket.org/lednesday/audiobook/src/master/ beginning December 2020.

Acknowledgments: This project was written as a course requirement for User Interfaces, taught by Anthony Hornof at the University of Oregon. Dr. Hornof provided input in the creation of this project.
I could not have possibly done this assignment without extensive help from Chris Jones. This is the biggest program I've ever written.

Changelog: I don't really know what this is for yet. 

Todo:  Focus mode still needs to be implemented. Several parts of read mode don't work right. When I use wav format, get_pos() doesn't work, so skipping forward and back causes a crash, and the user's position isn't really saved. When I use mp3 or ogg format, I get an error that the audio format isn't recognized. This makes no sense, but I haven't been able to fix it. User preferences is currently a stub, so skip interval can't be changed.